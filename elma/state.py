import struct
from elma.utils import null_padded

STATE_DAT_VERSION = 200
STATE_DAT_SHAREWARE = 0x75B6CB0
STATE_DAT_REGISTERED = 0x75B6D1D


def printTime(time):
    """
    Converts time in hundredths of a second into display format.
    """
    hundreds = time % 100
    time = (time-hundreds)/100
    seconds = time % 60
    minutes = (time-seconds)/60
    return '%02d:%02d:%02d' % (minutes, seconds, hundreds)


def decodeEncode(data):
    """
    Encode or Decode State.dat chunk
    """
    def sign16(value):
        value = value & 0xFFFF
        if(value & 0x8000):
            return -0x10000 + value
        return value

    def cmod(quo, div):
        rem = quo % div
        if(quo < 0):
            rem -= div
        return rem
    data = bytearray(data)
    eax = 23
    ecx = 9782
    for i in range(len(data)):
        data[i] ^= eax & 0xFF
        ecx = ecx + cmod(eax, 3391)*3391
        ecx = sign16(ecx)
        eax = ecx*31 + 3391
        eax = sign16(eax)
    return data


class State(object):
    """
    Represent a state.dat file

    Attributes:
        intTopTen (levelTop10[90]): List of top 10 objects for the 55 internal
            levels and 35 empty placeholder internals levels
        players (Player[50]): List of up to 50 players
        numPlayers (int): The number of valid players
        playerAName (string): Player A's profile name (15 characters max)
        playerBName (string): Player B's profile name (15 characters max)
        soundEnabled (bool): Menu setting
        soundCompatibility (bool): Menu setting
        isSinglePlayer (bool): Menu setting
        flagTag (bool): Menu setting
        unswappedBike (bool): Menu setting. True if bike is not swapped.
        highDetail (bool): Menu setting
        animatedObjects (bool): Menu setting
        animatedMenus (bool): Menu setting
        pAKeys (playerKeys): Player A's key codes
        pBKeys (playerKeys): Player B's key codes
        lastEditedLevName (string): Selection in editor (max 20 characters),
            e.g. "EXAMPLE.LEV"
        lastExternal (string): External file last chosen (max 20 characters),
            e.g. "example.lev"
        isRegistered (bool): Whether the state.dat comes from a registered or
            shareware copy of the game
    """
    def __init__(self):
        self.intTopTen = [LevelTop10() for _ in range(90)]
        self.players = [Player() for _ in range(50)]
        self.numPlayers = 0
        self.playerAName = ''
        self.playerBName = ''
        self.soundEnabled = True
        self.soundCompatibility = False
        self.isSinglePlayer = True
        self.flagTag = False
        self.unswappedBike = True
        self.highDetail = True
        self.animatedObjects = True
        self.animatedMenus = True
        self.pAKeys = PlayerKeys()
        self.pAKeys.resetA()
        self.pBKeys = PlayerKeys()
        self.pBKeys.resetB()
        self.incScreenSizeKey = 13
        self.decScreenSizeKey = 12
        self.screenShotKey = 23
        self.lastEditedLevName = ''
        self.lastExternal = ''
        self.isRegistered = True

    def __repr__(self):
        return (
            ('State:\nnumPlayers: %d\nplayerAName: %s\nplayerBName: %s\n' +
             'soundEnabled: %s\nsoundCompatibility: %s\nisSinglePlayer: %s\n' +
             'flagTag: %s\nunswappedBike: %s\nhighDetail: %s\n' +
             'animatedObjects: %s\nanimatedMenus: %s\nincScreenSizeKey: %d\n' +
             'decScreenSizeKey: %d\nscreenShotKey: %d\nlastEditedLevName: %s' +
             '\nlastExternal: %s\nisRegistered: %s') %
            (self.numPlayers, self.playerAName, self.playerBName,
             self.soundEnabled, self.soundCompatibility, self.isSinglePlayer,
             self.flagTag, self.unswappedBike, self.highDetail,
             self.animatedObjects, self.animatedMenus, self.incScreenSizeKey,
             self.decScreenSizeKey, self.screenShotKey, self.lastEditedLevName,
             self.lastExternal, self.isRegistered))


class LevelTop10(object):
    """
    Represent a level top 10 object

    Attributes:
        singleTop10 (Top10): top 10 for single player
        multiTop10 (Top10): top 10 for multi player
    """
    def __init__(self):
        self.singleTop10 = Top10()
        self.multiTop10 = Top10()


class Top10(object):
    """
    Represent a top 10 object

    Attributes:
        numberOfTimes (int): The number of times in the top10 list.
        times (int[10]): A list of times in hundredths of seconds.
        playerANames[10] (string): A list of player A names (15 characters per
            name max)
        playerBNames[10] (string): A list of player B names (15 characters per
            name max)
    """
    def __init__(self):
        self.numberOfTimes = 0
        self.times = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.playerANames = ['', '', '', '', '', '', '', '', '', '']
        self.playerBNames = ['', '', '', '', '', '', '', '', '', '']

    def __repr__(self):
        repr = 'Top10: %d elements\n' % self.numberOfTimes
        for i in range(self.numberOfTimes):
            repr += (
                '%s %s - %s\n' %
                (self.playerANames[i], self.playerBNames[i],
                 printTime(self.times[i])))
        return repr


class Player(object):
    """
    Represent a state.dat player profile

    Attributes:
        name (string): The profile name (max 16 characters)
        skips (bool[92]): A boolean list of whether the internal level has been
            skipped (55 levels + 37 placeholder levels)
        lastInternalIndex (int): The last internal level that the player is
            allowed to access (0-54)
        selectedInternalIndex (int): The level to display when clicking on
            "Play". -1 corresponds to External File.
    """
    def __init__(self):
        self.name = ''
        self.skips = [False for _ in range(92)]
        self.lastInternalIndex = 0
        self.selectedInternalIndex = 0

    def __repr__(self):
        repr = (
            'Player: (%s)\nlastInt=%d, selectedInt=%d' %
            (self.name, self.lastInternalIndex, self.selectedInternalIndex))
        for i in range(92):
            if(self.skips[i]):
                repr += '\nSkip %d' % i
        return repr


class PlayerKeys(object):
    """
    Represent a set of key codes for a player
    0 = unassigned keycode (= ???)

    Attributes:
        throtte (int): keycode
        brake (int): keycode
        rotateRight (int): keycode
        rotateLeft (int): keycode
        changeDir (int): keycode
        toggleNavigator (int): keycode
        toggleTime (int): keycode
        toggleShowHide (int): keycode
    """
    def resetA(self):
        self.throttle = 200
        self.brake = 208
        self.rotateRight = 205
        self.rotateLeft = 203
        self.changeDir = 57
        self.toggleNavigator = 47
        self.toggleTime = 20
        self.toggleShowHide = 2

    def resetB(self):
        self.throttle = 76
        self.brake = 80
        self.rotateRight = 81
        self.rotateLeft = 79
        self.changeDir = 82
        self.toggleNavigator = 48
        self.toggleTime = 21
        self.toggleShowHide = 3

    def __init__(self):
        self.resetA()

    def __repr__(self):
        return (
            ('playerKeys:\nthrottle: %d\nbrake: %d\nrotateRight: %d\n' +
             'rotateLeft: %d\nchangeDir: %d\ntoggleNavigator: %d\n' +
             'toggleTime: %d\ntoggleShowHide: %d') %
            (self.throttle, self.brake, self.rotateRight, self.rotateLeft,
             self.changeDir, self.toggleNavigator, self.toggleTime,
             self.toggleShowHide)
            )


def openStateDat(file):
    """
    Open state.dat file
    """
    def unpackLevelTop10(data):
        leveltop10 = LevelTop10()
        leveltop10.singleTop10 = unpackTop10(data[0:344])
        leveltop10.multiTop10 = unpackTop10(data[344:688])
        return leveltop10

    def unpackTop10(data):
        top10 = Top10()
        top10.numberOfTimes = struct.unpack('I', data[0:4])[0]
        top10.times = [struct.unpack('I', data[4+i*4:4+i*4+4])[0]
                       for i in range(10)]
        top10.playerANames = [
            data[44+i*15:44+i*15+15].split(b'\0')[0].decode('latin1')
            for i in range(10)]
        top10.playerBNames = [
            data[194+i*15:194+i*15+15].split(b'\0')[0].decode('latin1')
            for i in range(10)]
        return top10

    def unpackPlayer(data):
        player = Player()
        player.name = data[0:16].split(b'\0')[0].decode('latin1')
        player.skips = [struct.unpack('?', data[16+i:16+i+1])[0]
                        for i in range(92)]
        player.lastInternalIndex = struct.unpack('i', data[108:112])[0]
        player.selectedInternalIndex = struct.unpack('i', data[112:116])[0]
        return player

    def unpackPlayerKeys(data):
        playerKeys = PlayerKeys()
        playerKeys.throttle = struct.unpack('I', data[0:4])[0]
        playerKeys.brake = struct.unpack('I', data[4:8])[0]
        playerKeys.rotateRight = struct.unpack('I', data[8:12])[0]
        playerKeys.rotateLeft = struct.unpack('I', data[12:16])[0]
        playerKeys.changeDir = struct.unpack('I', data[16:20])[0]
        playerKeys.toggleNavigator = struct.unpack('I', data[20:24])[0]
        playerKeys.toggleTime = struct.unpack('I', data[24:28])[0]
        playerKeys.toggleShowHide = struct.unpack('I', data[28:32])[0]
        return playerKeys

    state = State()
    with open(file, "rb") as f:
        assert struct.unpack('I', decodeEncode(f.read(4)))[0] == STATE_DAT_VERSION
        chunk = decodeEncode(f.read(61920))
        state.intTopTen = [unpackLevelTop10(chunk[i*688:i*688+688]) for i in range(90)]
        chunk = decodeEncode(f.read(5800))
        state.players = [unpackPlayer(chunk[i*116:i*116+116]) for i in range(50)]
        state.numPlayers = struct.unpack('I', decodeEncode(f.read(4)))[0]
        state.playerAName = decodeEncode(f.read(15)).split(b'\0')[0].decode('latin1')
        state.playerBName = decodeEncode(f.read(15)).split(b'\0')[0].decode('latin1')
        state.soundEnabled = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.soundCompatibility = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.isSinglePlayer = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.flagTag = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.unswappedBike = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.highDetail = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.animatedObjects = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.animatedMenus = bool(struct.unpack('I', decodeEncode(f.read(4)))[0])
        state.pAKeys = unpackPlayerKeys(decodeEncode(f.read(32)))
        state.pBKeys = unpackPlayerKeys(decodeEncode(f.read(32)))
        state.incScreenSizeKey = struct.unpack('I', decodeEncode(f.read(4)))[0]
        state.decScreenSizeKey = struct.unpack('I', decodeEncode(f.read(4)))[0]
        state.screenShotKey = struct.unpack('I', decodeEncode(f.read(4)))[0]
        state.lastEditedLevName = decodeEncode(f.read(20)).split(b'\0')[0].decode('latin1')
        state.lastExternal = decodeEncode(f.read(20)).split(b'\0')[0].decode('latin1')
        chunk = struct.unpack('I', f.read(4))[0]
        if(chunk == STATE_DAT_SHAREWARE):
            state.isRegistered = False
        elif(chunk == STATE_DAT_REGISTERED):
            state.isRegistered = True
        else:
            assert False
    return state


def saveStateDat(state, file):
    """
    Save state.dat object to file
    """

    def packLevelTop10(levelTop10):
        return b''.join([
            packTop10(levelTop10.singleTop10),
            packTop10(levelTop10.multiTop10)
        ])

    def packTop10(top10):
        return b''.join(
            [struct.pack('I', top10.numberOfTimes)] +
            [struct.pack('I', top10.times[i]) for i in range(10)] +
            [null_padded(top10.playerANames[i], 15) for i in range(10)] +
            [null_padded(top10.playerBNames[i], 15) for i in range(10)]
        )

    def packPlayer(player):
        return b''.join(
            [null_padded(player.name, 16)] +
            [struct.pack('?', player.skips[i]) for i in range(92)] +
            [struct.pack('i', player.lastInternalIndex),
             struct.pack('i', player.selectedInternalIndex)]
        )

    def packPlayerKeys(playerKeys):
        return b''.join([
            struct.pack('I', playerKeys.throttle),
            struct.pack('I', playerKeys.brake),
            struct.pack('I', playerKeys.rotateRight),
            struct.pack('I', playerKeys.rotateLeft),
            struct.pack('I', playerKeys.changeDir),
            struct.pack('I', playerKeys.toggleNavigator),
            struct.pack('I', playerKeys.toggleTime),
            struct.pack('I', playerKeys.toggleShowHide)
        ])

    with open(file, "wb") as f:
        f.write(decodeEncode(struct.pack('I', 200)))
        f.write(decodeEncode(b''.join([packLevelTop10(state.intTopTen[i]) for i in range(90)])))
        f.write(decodeEncode(b''.join([packPlayer(state.players[i]) for i in range(50)])))
        f.write(decodeEncode(struct.pack('I', state.numPlayers)))
        f.write(decodeEncode(null_padded(state.playerAName, 15)))
        f.write(decodeEncode(null_padded(state.playerBName, 15)))
        f.write(decodeEncode(struct.pack('I', state.soundEnabled)))
        f.write(decodeEncode(struct.pack('I', state.soundCompatibility)))
        f.write(decodeEncode(struct.pack('I', state.isSinglePlayer)))
        f.write(decodeEncode(struct.pack('I', state.flagTag)))
        f.write(decodeEncode(struct.pack('I', state.unswappedBike)))
        f.write(decodeEncode(struct.pack('I', state.highDetail)))
        f.write(decodeEncode(struct.pack('I', state.animatedObjects)))
        f.write(decodeEncode(struct.pack('I', state.animatedMenus)))
        f.write(decodeEncode(packPlayerKeys(state.pAKeys)))
        f.write(decodeEncode(packPlayerKeys(state.pBKeys)))
        f.write(decodeEncode(struct.pack('I', state.incScreenSizeKey)))
        f.write(decodeEncode(struct.pack('I', state.decScreenSizeKey)))
        f.write(decodeEncode(struct.pack('I', state.screenShotKey)))
        f.write(decodeEncode(null_padded(state.lastEditedLevName, 20)))
        f.write(decodeEncode(null_padded(state.lastExternal, 20)))
        f.write(struct.pack('I', STATE_DAT_REGISTERED if state.isRegistered
                            else STATE_DAT_SHAREWARE))
    return True
