from abc import ABCMeta
import random

class Obj2(object):
    """
    Represent an Elastomania 2 object

    Attributes:
        point (Point): The 2D Point that represents the position of the object.
        type (int): The type of the object, which should be one of:
            Obj2.FLOWER - Obj2.Teleport
        group_id (int): The Group ID that is activated by a Group Switch or the
            ID that will activate a Switch
        path_id (int): The Path ID activated by a Switch
        opener_closer (int): Determines whether a Switch opens or closes a path
            (Obj2.SWITCH_OPENER, Obj2.SWITCH_CLOSER), or whether a Turn is left
            or right (Obj2.TURN_LEFT, Obj2.TURN_RIGHT), or whether a teleport
            is a source or destination (Obj2.TELEPORT_A, Obj2.TELEPORT_B)
        card (int): Determines the colour of a card for Card, Switch and Group
            Switch objects
        condition_path_id (int): Determines Condition 2: Path ID for Group
            Switch objects.
        condition_state (int): Determines Condition 2: State for Group Switch
            objects.
        platform_id (int): Determines the Platform ID for Platform Reference
            objects.
        comments (string): A comment section to add extra information for the
            editor when designing the level. Supports up to 300 bytes of text.
    """

    FLOWER = 1
    FOOD = 2
    KILLER = 3
    START = 4
    BONUS = 5
    CARD = 6
    SWITCH = 7
    GROUP_SWITCH = 8
    PLATFORM_REFERENCE = 9
    STAGE = 10
    TURN = 11
    JUMP = 12
    TELEPORT = 13

    NO_CARD = 0
    RED = 1
    YELLOW = 2
    BLUE = 3
    
    UP = 1
    DOWN = 0
    
    SWITCH_OPENER = 1
    SWITCH_CLOSER = 0
    TURN_LEFT = 1
    TURN_RIGHT = 0
    TELEPORT_A = 1
    TELEPORT_B = 0

    def __init__(self, point, type,
                 group_id=0, path_id=0, opener_closer=OPENER, card=NO_CARD, 
                 condition_path_id=0, condition_state=UP, platform_id=0, comments=''):
        self.point = point
        self.type = type
        self.group_id = group_id
        self.path_id = path_id
        self.opener_closer = opener_closer
        self.card = card
        self.condition_path_id = condition_path_id
        self.condition_state = condition_state
        self.platform_id = platform_id
        self.comments = comments

    def __repr__(self):
        if(self.type==FLOWER):
            return (
                'Obj2(point: %s, type: Flower, comments: %s)' %
                (self.point, self.comments))
        if(self.type==FOOD):
            return (
                'Obj2(point: %s, type: Food)' %
                (self.point, self.comments))
        if(self.type==KILLER):
            return (
                'Obj2(point: %s, type: Killer)' %
                (self.point, self.comments))
        if(self.type==START):
            return (
                'Obj2(point: %s, type: Start)' %
                (self.point, self.comments))
        if(self.type==BONUS):
            return (
                'Obj2(point: %s, type: Bonus)' %
                (self.point, self.comments))
        if(self.type==CARD):
            return (
                'Obj2(point: %s, type: Bonus, card: %s)' %
                (self.point, self.card, self.comments))
        if(self.type==SWITCH):
            return (
                'Obj2(point: %s, type: Switch, group_id: %s, opener_closer: %s, path_id: %s, card: %s)' %
                (self.point, self.group_id, self.path_id, self.opener_closer, self.card, self.comments))
        if(self.type==GROUP_SWITCH):
            return (
                'Obj2(point: %s, type: Group Switch, group_id: %s, card: %s, condition_path_id: %s, condition_state: %s)' %
                (self.point, self.group_id, self.card, self.condition_path_id, self.condition_state, self.comments))
        if(self.type==PLATFORM_REFERENCE):
            return (
                'Obj2(point: %s, type: Platform Reference, platform_id: %s)' %
                (self.point, self.platform_id, self.comments))
        if(self.type==STAGE):
            return (
                'Obj2(point: %s, type: Stage)' %
                (self.point, self.comments))
        if(self.type==TURN):
            return (
                'Obj2(point: %s, type: Turn, opener_closer: %s)' %
                (self.point, self.opener_closer, self.comments))
        if(self.type==JUMP):
            return (
                'Obj2(point: %s, type: Jump)' %
                (self.point, self.comments))
        if(self.type==TELEPORT):
            return (
                'Obj2(point: %s, type: Teleport, group_id: %s, opener_closer: %s)' %
                (self.point, self.group_id, self.opener_closer, self.comments))
        return (
            'Obj2(point: %s, type: %s, group_id: %s, path_id: %s, opener_closer: %s, card: %s, condition_path_id: %s, condition_state: %s, platform_id: %s)' %
            (self.point, self.type, self.path_id, self.opener_closer, self.card, self.condition_path_id, self.condition_state, self.platform_id, self.comments))
            
    def __eq__(self, other_obj):
        return (self.point == other_obj.point and
                self.type == other_obj.type and
                self.group_id == other_obj.group_id and
                self.path_id == other_obj.path_id and
                self.opener_closer == other_obj.opener_closer and
                self.card == other_obj.card and
                self.condition_path_id == other_obj.condition_path_id
                self.platform_id == other_obj.platform_id
                self.comments == other_obj.comments)


class Polygon2(object):
    """
    Represents an Elastomania 2 polygon.

    Attributes:
        points (list): A list of Points defining the polygon contour.
        type (int): Polygon type (NORMAL - BORDER)
        main_id (int): Platform's Platform ID or Path's Path ID
        style_index (int): Normal/Platform/Border's Style IndexError
        override_force_solid (int): Override - Polygon interacts with biker
            OVERRIDE_FORCE_SOLID_ON or NO_OVERRIDE
        override_border (int): Override - Border on, Border off, and Whole
            polygon border colored. NO_OVERRIDE of OVERRIDE_BORDER_ON/OFF/COLOR
        override_distance (int): New distance, or 0 for default style distance
        path_sub_type (int): Path Sub Type (DOOR - CONTINUOUS_TWOWAY)
        path_platform_id (int): Path Platform ID
        path_speed_at_top (int): Path Speed At Top or Path Speed
        path_speed_at_bottom (int): Path Speed At Bottom
        path_open_delay (int): Path Open Delay
        path_close_delay (int): Path Close Delay
        path_reopen_delay (int): Path ReOpen Delay
        path_reclose_delay (int): Path ReClose Delay
        path_initially (int): Path Initially - initial position described by
            INITIALLY_UP or INITIALLY_DOWN
        path_no_of_platforms (int): Path Number of Platforms
        comments (string): A comment section to add extra information for the
            editor when designing the level. Supports up to 300 bytes of text.
    """
    
    NORMAL = 1
    INACTIVE = 2
    PLATFORM = 3
    PATH = 4
    BORDER = 5
    
    NO_OVERRIDE = 0
    OVERRIDE_FORCE_SOLID_ON = 1
    
    NO_OVERRIDE = 0
    OVERRIDE_BORDER_ON = 1
    OVERRIDE_BORDER_OFF = 2
    OVERRIDE_BORDER_COLOR = 3
    
    DOOR = 1
    ELEVATOR = 2
    FALLING = 3
    CONTINUOUS_ONEWAY = 4
    CONTINUOUS_TWOWAY = 5
    
    INITIALLY_UP = 1
    INITIALLY_DOWN = 0
    
    def __init__(self, points, type, main_id=0, style_index=0, override_force_solid=NO_OVERRIDE, override_border=NO_OVERRIDE, 
                 override_distance=0, path_sub_type=0, path_platform_id=0, path_speed_at_top=0, path_speed_at_bottom=0,
                 path_open_delay=0, path_close_delay=0, path_reclose_delay=0, path_initially=0, path_no_of_platforms=0, comments=''):
        self.points = points
        self.type = type
        self.main_id = main_id
        self.style_index = style_index
        self.override_force_solid = override_force_solid
        self.override_border = override_border
        self.override_distance = override_distance
        self.path_sub_type = path_sub_type
        self.path_platform_id = path_platform_id
        self.path_speed_at_top = path_speed_at_top
        self.path_speed_at_bottom = path_speed_at_bottom
        self.path_open_delay = path_open_delay
        self.path_close_delay = path_close_delay
        self.path_reopen_delay = path_reopen_delay
        self.path_reclose_delay = path_reclose_delay
        self.path_initially = path_initially
        self.path_no_of_platforms = path_no_of_platforms
        self.comments = comments

    def __repr__(self):
        if(self.type == PATH):
            return (
                'Polygon2(point: %s, type: %s, main_id: %s, path_sub_type: %s, path_platform_id: %s, path_speed_at_top: %s, path_speed_at_bottom: %s, path_open_delay:%, path_close_delay: %s, path_reopen_delay: %s, path_reclose_delay: %s, path_initially: %s, path_no_of_platforms: %s, comments: %s)' %
                (self.point, self.type, self.main_id, self.path_sub_type, self.path_platform_id, self.path_speed_at_top, self.path_speed_at_bottom, self.path_open_delay, self.path_close_delay, self.path_reopen_delay, self.path_reclose_delay, self.path_initially, self.path_no_of_platforms, self.comments))
        else:
            return (
                'Polygon2(point: %s, type: %s, main_id: %s, style_index: %s, override_force_solid: %s, override_border: %s, override_distance: %s, comments: %s)' %
                (self.point, self.type, self.main_id, self.style_index, self.override_force_solid, self.override_border, self.override_distance, self.comments))
                
class Line(object):
    """
    Represent an Elastomania 2 line

    Attributes:
        point1 (Point): The 2D Point that represents the initial coordinate
        point2 (Point): The 2D Point that represents the destination coordinate
        texture_id (int): The texture id to draw to the right of the line. If
            the right of the line is "sky"/nothing to draw, the texture_id
            should be NO_TEXTURE. This will also draw nothing in the minimap.
            NO_TEXTURE - YELLOW
        solid (boolean): Whether the kuski can touch the line
        distance (int): Distance
        invisible (boolean): Note that the line will still be drawn in the
            minimap however. In general, in normal circumstances, invisible
            lines should not be included as a Line object at all. This property
            is unused by the default editor as the lines are simply not
            included, or the texture_id is set as NO_TEXTURE instead
    """

    #texture_id cheat sheet: http://kopasite.net/up/2/StyleCheatSheet.pdf
    NO_TEXTURE = 0
    GROUND_BORDER = 10
    YELLOW_STRIPES = 11
    GROUND = 100
    GREEN = 101
    PLATFORM = 102
    BLUE_SKY = 103
    PLATFORM_BORDER = 104
    GROUND_VIEW = 105
    SKY_VIEW = 106
    HELMET = 120
    YELLOW_EDGE = 122
    YELLOW = 124
    
    def __init__(self, point1, point2, texture_id, solid, distance, invisible=False):
        self.point1 = point1
        self.point2 = point2
        self.texture_id = texture_id
        self.solid = solid
        self.distance = distance
        self.invisible = invisible

    def __repr__(self):
        return (
            'Line(point1: %s, point2: %s, texture_id: %s, solid: %s, distance: %s, invisible: %s)' %
            (self.point1, self.point2, self.texture_id, self.solid, self.distance, self.invisible))
            
    def __eq__(self, other_obj):
        return (self.point1 == other_obj.point1 and
                self.point2 == other_obj.point2 and
                self.texture_id == other_obj.texture_id and
                self.solid == other_obj.solid and
                self.distance == other_obj.distance and
                self.invisible == other_obj.invisible)

class Path(object):
    """
    Represent an Elastomania 2 platform

    Attributes:
        lines (list): A list of lines drawing all polygons of the corresponding
            platform_id (even if several polygons), with distance=0 and
            invisible=False
        platform_reference_point: A Point object of the object's xy
        int112-156 (int): See http://wiki.elmaonline.net/Elma_2/Technical#Platform_Object
        platform_id (int): Platform Id
        texture_id (int): Main Texture ID of the platform
    """

    DOOR = 1
    ELEVATOR = 2
    FALLING = 3
    CONTINUOUS_ONEWAY = 4
    CONTINUOUS_TWOWAY = 5
    
    INITIALLY_UP = 1
    INITIALLY_DOWN = 0
    
    def __init__(self):
        self.path_id = 0
        self.path_platform_id = 0
        self.path_sub_type = 0
        self.path_speed_at_top = 0
        self.path_speed_at_bottom = 0
        self.path_open_delay = 0
        self.path_close_delay = 0
        self.path_reopen_delay = 0
        self.path_reclose_delay = 0
        self.path_initially = 0
        self.path_no_of_platforms = 0
        self.forward_frames = 0
        self.total_frames = 0
        int96 = 0
        int100 = 0
        int104 = 0
        int108 = 0
        int112 = 0
        int136 = 0
        movement_x = []
        movement_y = []
        velocity_x = []
        velocity_y = []
        platform_x = []
        platform_y = []

    def __repr__(self):
        return (
            'Path(path_id: %s, path_sub_type: %s, path_platform_id: %s, path_speed_at_top: %s, path_speed_at_bottom: %s, path_open_delay:%, path_close_delay: %s, path_reopen_delay: %s, path_reclose_delay: %s, path_initially: %s, path_no_of_platforms: %s)' %
            (sself.path_id, self.path_sub_type, self.path_platform_id, self.path_speed_at_top, self.path_speed_at_bottom, self.path_open_delay, self.path_close_delay, self.path_reopen_delay, self.path_reclose_delay, self.path_initially, self.path_no_of_platforms))
    
    def X_Min(self):
        return min(self.lines, key=lambda i: i.point1.x)
        
    def Y_Min(self):
        return min(self.lines, key=lambda i: i.point1.y)
        
    def X_Max(self):
        return max(self.lines, key=lambda i: i.point1.x)
        
    def Y_Max(self):
        return max(self.lines, key=lambda i: i.point1.y)

class Level2(object):
    """
    Represent an Elastomania 2 level.

    Attributes:
        polygons (list): A list of Polygon2 representing polygons in the level.
        objects (list): A list of Object2 representing objects in the level.
        pictures (list): A list of Picture representing pictures in the level.
        name (string): The name of level, which should be no longer than 50
            characters long. Not displayed in-game
        lgr (string): The name of the LGR used for this level, which should be
            no longer than 10 characters long. Always default in elma2 so far
        face_right (bool): Whether the bike should turn automatically at start
        
        topology_error (bool): If there is a topology error, all subsequent
            variables are excluded from the saved level file
        lines (list): A list of Line - solid and non-solid lines from Normal and
            Border polygons
        platforms (list): A list of Platform in the level
        paths (list): A list of Path in the level
        level_id (int): A unique POSITIVE SIGNED 32bit integer level identifier.
        unknown1 (float): Probably always 60.0
        unknown2 (float): Unknown negative number always around this number
        unknown3 (float): Unknown negative number always around this number
    """
    def __init__(self):
        self.polygons = []
        self.objects = []
        self.pictures = []
        self.name = 'Unnamed'
        self.lgr = 'default'
        self.face_right=False
        
        self.topology_error=False
        self.lines = []
        self.platforms = []
        self.paths = []
        self.level_id = random.randint(0, (2 ** 31) - 1)
        self.unknown1 = 60.0
        self.unknown2 = -406.64166666666665
        self.unknown3 = -74.99166666666666 
        
    def __repr__(self):
        return (('Level(level_id: %s)') %
                (self.level_id))


