EXTERNAL_FILE=-1
WARM_UP=0
FLAT_TRACK=1
TWIN_PEAKS=2
OVER_AND_UNDER=3
UPHILL_BATTLE=4
LONG_HAUL=5
HI_FLYER=6
TAG=7
TUNNEL_TERROR=8
THE_STEPPES=9
GRAVITY_RIDE=10
ISLANDS_IN_THE_SKY=11
HILL_LEGEND=12
LOOP_DE_LOOP=13
SERPENTS_TALE=14
NEW_WAVE=15
LABYRINTH=16
SPIRAL=17
TURNAROUND=18
UPSIDE_DOWN=19
HANGMAN=20
SLALOM=21
QUICK_ROUND=22
RAMP_FRENZY=23
PRECARIOUS=24
CIRCUITOUS=25
SHELF_LIFE=26
BOUNCE_BACK=27
HEADBANGER=28
PIPE=29
ANIMAL_FARM=30
STEEP_CORNER=32
ZIG_ZAG=32
BUMPEY_JOURNEY=33
LABYRINTH_PRO=34
FRUIT_IN_THE_DEN=35
JAWS=36
CURVACEOUS=37
HAIRCUT=38
DOUBLE_TROUBLE=39
FRAMEWORK=40
ENDURO=41
HE_HE=42
FREEFALL=43
SINK=44
BOWLING=45
ENIGMA=46
DOWNHILL=47
WHAT_THE_HECK=48
EXPERT_SYSTEM=49
TRICKS_ABOUND=50
HANG_TIGHT=51
HOOKED=52
APPLE_HARVEST=53
MORE_LEVELS=54