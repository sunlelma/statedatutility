Sunl's State.Dat Utilities (v6)

1) Import all your best times from elma.online, moposite, elmastats, elmaclub or vk0.dk into a merge.dat file you can then merge to your local Elma.exe game
2) Unlock all levels for a player
3) Unzip and zip state.dat to manually change times


STATE JSON INFO:

You can unzip state.dat into a JSON file.

The generated JSON can be edited with any JSON editor. Non-programmers can easily use this website to edit the state.json: https://jsoneditoronline.org

Don't delete any entries. You can only edit the values or set the value to 0 / nothing. If you delete any entry, the json will create a glitched state.dat

intTopTen: 55 internal levels (and 35 empty placeholder internals levels that you can ignore)
	0 = Warm Up
	1 = Flat Track
	2 = Twin Peaks
	...
	53 = Apple Harvest
	54 = More Levels
	55-90 = nothing


VERSION HISTORY:

2023/10/13
Version 6: ElmaStats switched to https

2021/9/14
Version 5: Added ability to import times from vk0.dk

2021/9/13
Version 4: Updated from elmaonline.net to elma.online

2020/6/15
Version 3: Added ability to import times from Moposite, ElmaClub and ElmaStats

2020/5/13
Version 2: Added Unzip and zip ability
