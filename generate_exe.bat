@echo This will delete dist/ and build/ then generate the distributable program in dist/
@pause
rmdir dist /s /q
rmdir build /s /q
rmdir out /s /q
rmdir __pycache__ /s /q

pyinstaller --onefile StateDatUtility.py
@echo COMMENT --add-binary "C:\Programs\Python 3.9.2\python39.dll";"."

copy /y readme_dist.txt dist\readme.txt

@pause