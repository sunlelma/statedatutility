@echo This will delete dist/ and build/
@pause

rmdir dist /s /q
rmdir build /s /q
rmdir out /s /q
rmdir __pycache__ /s /q

pause