import os
from elma.state import *
from elma.levelnames import *
import json
import jsonpickle
import urllib.request
import re
import requests

intIdsEOL = [2,4,5,6,7,8,9,10,15,59,78,109,139,219,71,51,165,57,128,197,43,107,98,100,175,192,38,198,31,16,18,164,66,131,156,357,45,13,408,412,24,416,415,95,29,33,46,21,52,257,135,133,413,17,39]
def buildStateElmaOnline(eolname,elmaname):
    state=State()
    print('Parsing EOL...')
    with urllib.request.urlopen('https://api.elma.online/api/player/kuski/%s'%(eolname)) as url:
        data = json.loads(url.read().decode())
        try:
            eolname_id = data['KuskiIndex']
        except:
            print('Error finding Kuski %s on website'%(eolname))
    for i in range(55):
        print(i+1)
        with urllib.request.urlopen('https://api.elma.online/api/allfinished/%s/%s/10'%(intIdsEOL[i],eolname_id)) as url:
            data = json.loads(url.read().decode())
            entries=len(data)
            state.intTopTen[i].singleTop10.numberOfTimes=entries
            for j in range(entries):
                state.intTopTen[i].singleTop10.playerANames[j]=elmaname
                state.intTopTen[i].singleTop10.playerBNames[j]='Copied from EOL'
                state.intTopTen[i].singleTop10.times[j]=data[j]['Time']
    if not(os.path.exists("out")):
        os.makedirs("out")
    saveStateDat(state,'out/merge.dat')
    print('Saved as out/merge.dat')
    print('Make a backup of your state.dat file.')
    print('Copy your merge.dat into your Elma/ folder. When you open the game, Elma.exe will automatically merge your state.dat with merge.dat')
    input('\nPress enter to exit...')
    
    

def buildStateMopo(eolname,elmaname):
    state=State()
    print('Parsing moposite website')
    with urllib.request.urlopen('https://moposite.com/records.php?m=r&wnt=world&a=10&l=all&p=%s&annual=none&historic=none'%eolname) as url:
        search = 0
        level = 0
        entries = 0
        for line in url:
            data = line.decode("utf-8")
            if(search == 0):
                if(data[0:23] != "<h2 class='subHeading'>"):
                    continue
                r = re.search(r"'subHeading'>(\d{1,2})\.",data)
                if(r):
                    level = int(r.group(1))-1
                    entries = 0
                    search = 1
                    print(level)
            elif(search == 1):
                if(data[0:8] == "</table>"):
                    search = 0
                    continue
                r = re.search(r"width4'>(\d:)?(\d\d),(\d\d)<\/td>",data)
                if(r):
                    value = (int(r.group(1)[:-1])*60*100 if r.group(1) else 0) +int(r.group(2))*100+int(r.group(3))
                    if(entries < 10):
                        state.intTopTen[level].singleTop10.playerANames[entries]=elmaname
                        state.intTopTen[level].singleTop10.playerBNames[entries]='Copied Moposite'
                        state.intTopTen[level].singleTop10.times[entries]=value
                        entries += 1
                        state.intTopTen[level].singleTop10.numberOfTimes=entries

    if not(os.path.exists("out")):
        os.makedirs("out")
    saveStateDat(state,'out/merge.dat')
    print('Saved as out/merge.dat')
    print('Make a backup of your state.dat file.')
    print('Copy your merge.dat into your Elma/ folder. When you open the game, Elma.exe will automatically merge your state.dat with merge.dat')
    input('\nPress enter to exit...')


def buildStateHoyer(eolname,elmaname):
    state=State()
    print('Parsing elmastats')
    with urllib.request.urlopen('https://stats.sshoyer.net/txt.php?u=%s'%eolname) as url:
        search = 0
        level = 0
        entries = 0
        for line in url:
            data = line.decode("utf-8")
            if(search == 0):
                if(data[0:5] != "Level"):
                    continue
                r = re.search(r"Level (\d{1,2}),",data)
                if(r):
                    level = int(r.group(1))-1
                    entries = 0
                    search = 1
                    print(level)
            elif(search == 1):
                if(len(data) == 2):
                    search = 0
                    continue
                r = re.search(r"(\d\d):(\d\d):(\d\d)",data)
                if(r):
                    value = int(r.group(1))*60*100 +int(r.group(2))*100+int(r.group(3))
                    if(entries < 10):
                        state.intTopTen[level].singleTop10.playerANames[entries]=elmaname
                        state.intTopTen[level].singleTop10.playerBNames[entries]='Copied ElmaStats'
                        state.intTopTen[level].singleTop10.times[entries]=value
                        entries += 1
                        state.intTopTen[level].singleTop10.numberOfTimes=entries

    if not(os.path.exists("out")):
        os.makedirs("out")
    saveStateDat(state,'out/merge.dat')
    print('Saved as out/merge.dat')
    print('Make a backup of your state.dat file.')
    print('Copy your merge.dat into your Elma/ folder. When you open the game, Elma.exe will automatically merge your state.dat with merge.dat')
    input('\nPress enter to exit...')


def buildStateClub(eolname,elmaname):
    state=State()
    print('Parsing elmaclub')
    
    opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor())
    response = opener.open('http://www.elmaclub.net/rus/stats/user/?pack=1&uid=%s'%eolname, timeout=30)
    
    search = 0
    level = 0
    entries = 0
    linecount = 0
    for line in response:
        data = line.decode("cp1251")
        if(search == 0):
            #if(data[0:23] != '		<td class="CTableStat">'):
            #    continue
            r = re.search(r'"CTableStat">(\d{1,2})\.<\/td>',data)
            if(r):
                level = int(r.group(1))-1
                entries = 0
                linecount = 0
                search = 1
                print(level)
        elif(search == 1):
            linecount += 1
            if(linecount == 4):
                search = 0
                continue
            if(linecount == 1):
                continue
            r = re.search(r"(\d\d):(\d\d):(\d\d)",data)
            if(r):
                value = int(r.group(1))*60*100 +int(r.group(2))*100+int(r.group(3))
                print(value)
                if(entries < 10):
                    state.intTopTen[level].singleTop10.playerANames[entries]=elmaname
                    state.intTopTen[level].singleTop10.playerBNames[entries]='Copied ElmaClub'
                    state.intTopTen[level].singleTop10.times[entries]=value
                    entries += 1
                    state.intTopTen[level].singleTop10.numberOfTimes=entries

    if not(os.path.exists("out")):
        os.makedirs("out")
    saveStateDat(state,'out/merge.dat')
    print('Saved as out/merge.dat')
    print('Make a backup of your state.dat file.')
    print('Copy your merge.dat into your Elma/ folder. When you open the game, Elma.exe will automatically merge your state.dat with merge.dat')
    input('\nPress enter to exit...')
    
def buildStateVK0(eolname,elmaname):
    state=State()
    print('Parsing vk0')
    url = 'http://www.vk0.dk/elmatimes.php'
    header = {
        'teamsort': '',
        'natsort': '',
        'sortteam': '',
        'sortnat': '',
        'kuski': eolname,
        'selectkuski': 'Submit+Query'
    }
    session = requests.session()
    req = requests.post(url, data=header)
    raw = req._content.decode("utf-8")
    
    raw = raw[raw.find('<font color=white> Next Target</td>'):]
    while True:
        r = re.search(r'<tr bgcolor=#......><td>(\d{1,2})</td><td>[^<]+</td><td align=center>([^<]+)</td>',raw)
        if(not r):
            break
        level = int(r.group(1))-1
        r2 = re.search(r'((\d+):)?(\d\d),(\d\d)',r.group(2))
        value = int(r2.group(2) or 0)*60*100 +int(r2.group(3))*100+int(r2.group(4))
        
        state.intTopTen[level].singleTop10.playerANames[0]=elmaname
        state.intTopTen[level].singleTop10.playerBNames[0]='Copied vk0.dk'
        state.intTopTen[level].singleTop10.times[0]=value
        state.intTopTen[level].singleTop10.numberOfTimes=1
        
        print("%s: %s"%(level,value))
        raw = raw[r.end():]
        
    if not(os.path.exists("out")):
        os.makedirs("out")
    saveStateDat(state,'out/merge.dat')
    print('Saved as out/merge.dat')
    print('Make a backup of your state.dat file.')
    print('Copy your merge.dat into your Elma/ folder. When you open the game, Elma.exe will automatically merge your state.dat with merge.dat')
    input('\nPress enter to exit...')
    
    
    
    
def unlockLevels(file,name):
    state=openStateDat(file)
    success=False
    for player in state.players:
        if(player.name == name):
            player.skips = [False for _ in range(92)]
            player.lastInternalIndex = 54
            print('Found player!')
            success=True
    if(success==False):
        print('Unable to find player')
    else:
        saveStateDat(state,'out/state.dat')
        print('Saved as out/state.dat')
        print('Make a backup of your state.dat and then use the the out/state.dat file')
    input('\nPress enter to exit...')
    
def fromJSON():
    if not(os.path.exists("out")):
        os.makedirs("out")
    with open("state.json",'r') as infile:
        b = jsonpickle.decode(infile.read())
        saveStateDat(b,"out/state.dat")
    print('Saved as out/state.dat')
    print('Make a backup of your state.dat and then use the the out/state.dat file')
    input('\nPress enter to exit...')
    
def toJSON():
    if not(os.path.exists("out")):
        os.makedirs("out")
    a = openStateDat("state.dat")
    with open("out/state.json", 'w') as outfile:
        outfile.write(jsonpickle.encode(a))
    print('Saved as out/state.json')
    print('Use a JSON editor to modify the file! (Recommended for non-programmers: https://jsoneditoronline.org)')
    input('\nPress enter to exit...')

print('Sunl\'s State.Dat utilities v5')
print('1. Import internal times state.dat from elma.online')
print('2. Import internal times state.dat from moposite.com')
print('3. Import internal times state.dat from stats.sshoyer.net (elmastats)')
print('4. Import internal times state.dat from elmaclub.net')
print('5. Import internal times state.dat from vk0.dk')
print('6. Unlock all levels for a player')
print('7. Unzip state.dat to state.json')
print('8. Zip state.json to state.dat')
print('0. Quit')
choice=input('Type a number\n')
if(choice=="1"):
    print('1. Import internal times state.dat from elma.online')
    eolname=input('What is your EOL player name?\n')
    elmaname=input('What is your Elma profile player name?\n')
    buildStateElmaOnline(eolname,elmaname)
elif(choice=="2"):
    print('2. Import internal times state.dat from moposite.com')
    eolname=input('What is your Moposite player name?\n')
    elmaname=input('What is your Elma profile player name?\n')
    buildStateMopo(eolname,elmaname)
elif(choice=="3"):
    print('3. Import internal times state.dat from stats.sshoyer.net (elmastats)')
    eolname=input('What is your elmastats player name?\n')
    elmaname=input('What is your Elma profile player name?\n')
    buildStateHoyer(eolname,elmaname)
elif(choice=="4"):
    print('4. Import internal times state.dat from elmaclub.net')
    eolname=input('What is your elmaclub ID or UID (check in your profile url, e.g. Abula is 271)?\n')
    elmaname=input('What is your Elma profile player name?\n')
    buildStateClub(eolname,elmaname)
elif(choice=="5"):
    print('5. Import internal times state.dat from vk0.dk')
    eolname=input('What is your vk0 player name?\n')
    elmaname=input('What is your Elma profile player name?\n')
    buildStateVK0(eolname,elmaname)
elif(choice=="6"):
    print('6. Unlock all levels for a player')
    print('Make sure to copy your state.dat into the same folder as this .exe')
    elmaname=input('What is your Elma profile player name?\n')
    unlockLevels('state.dat',elmaname)
elif(choice=="7"):
    print('7. Unzip state.dat to state.json')
    print('Make sure to copy your state.dat into the same folder as this .exe')
    input('Press enter to continue\n')
    toJSON()
elif(choice=="8"):
    print('8. Zip state.json to state.dat')
    print('Make sure to copy your state.json into the same folder as this .exe')
    input('Press enter to continue\n')
    fromJSON()
else:
    print('0. Quit')
    input('Press enter to exit...\n')